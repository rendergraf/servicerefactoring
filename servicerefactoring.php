<?php

public function postConfirm(){
        $id = Input::get('service_id');
        if($id){
            $servicio = Service::find($id);
            if ($servicio){
                if ($servicio->status_id == '6'){
                    return Response::json(array('error' => '2');
                }
                if ($servicio->driver_id == NULL && $servicio->status_id == '1'){
                    $driverId = Input::get('driver_id');
                    if($driverId){
                        $driver = Driver::find(Input::get('driver_id'));
                        $driver->available = '0';
                        $driver->save();

                        $servicio->driver_id = Input::get('driver_id');
                        $servicio->status_id = '2';
                        $servicio->car_id = $driver->card_id;
                        $servicio->save();

                        $pushMessage = 'Tu servicio ha sido confirmado';
                        $push = Push::make();
                        if ($servicio->user->uuid == ''){
                            return Response::json(array('error' => '0'));
                        }
                        if($servicio->user->type == '1'){
                            $result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
                        }
                        else{
                            $result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default', 'Open', array('serviceId' => $servicio->id));
                        }
                        if($result){
                            return Response::json(array('error' => '0'));
                        }
                        else{
                            return Response::json(array('error' => '4'));
                        }
                    }
                    else{
                        return Response::json(array('error' => '6'));
                    }
                }
                else{
                    return Response::json(array('error' => '1'));
                }
            }
            else{
                return Response::json(array('error' => '3'));
            }
        }
        else{
            return Response::json(array('error' => '5'));
        }
    }